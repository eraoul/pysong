"""Represents a measure of music."""

from itertools import accumulate
from typing import List, Optional

from pysong.core.time_signature import TimeSignature
from pysong.core.note import Note
from pysong.core.note_sequence import NoteSequence

from pysong.core.types import NoteLike


class Measure:
    """Represents a measure of music. Has an optional start time with respect to the musical score.
    """

    def __init__(self,
                 time_signature: TimeSignature = None,
                 notes: NoteSequence = None,
                 start_time_quarters: float = None):
        if not time_signature:
            self._time_signature = TimeSignature()
        else:
            self._time_signature = time_signature

        if not notes:
            self._notes = NoteSequence()
        else:
            self._notes = notes

        self._start_time_quarters = start_time_quarters

    def __len__(self):
        return len(self._notes)

    def __iter__(self):
        return self._notes.__iter__()

    def __getitem__(self, key):
        return self._notes[key]

    @property
    def time_signature(self) -> TimeSignature:
        """Returns the TimeSignature of this measure."""
        return self._time_signature

    @property
    def notes(self) -> NoteSequence:
        """Returns the NoteSequence contained in this measure."""
        return self._notes

    @property
    def start_time_quarters(self) -> Optional[float]:
        """Returns the start time of this measure relative to the score, or None if not known."""
        return self._start_time_quarters

    @start_time_quarters.setter
    def start_time_quarters(self, start_time_quarters: float):
        """Set the start time of this measure, in quarters relative to the start of the score."""
        self._start_time_quarters = start_time_quarters

    def append_note(self, note: Note):
        """Append a note to the end of this measure."""
        self.notes.append(note)

    @property
    def duration(self) -> float:
        """The total duration of the measure, in quarters."""
        return sum(n.duration.quarters for n in self.notes)

    @property
    def notes_with_start_times(self) -> List[Note]:
        """Constructs a new list of note objects with start times.

        If the measure start time is defined, this is added to the start time of each note to return the score position.
        Otherwise, the start times are relative to the start of the measure.
        """
        start_times = accumulate(
            self.notes[:-1],  # skip last note; we don't need its duration.
            lambda s, note: s + note.duration.quarters,
            initial=self.start_time_quarters)
        notes_and_starts = zip(self.notes, start_times)
        return [Note.from_note_with_start_time(n, start) for n, start in notes_and_starts]

    def __repr__(self):
        s = f'{self.time_signature}: '
        s += ' '.join(str(n) for n in self.notes)

        return s

""""Represent a sequence of notes. Used in a measure or an unmeasured motif, etc."""

from math import ceil
from typing import Iterable, List

from music21 import stream
from music21.clef import Clef as Music21Clef
from music21.clef import BassClef, TrebleClef
from music21.note import Note as Music21Note
from music21.note import Rest as Music21Rest
from music21.duration import Duration as Music21Duration
from music21.chord import Chord as Music21Chord
from music21.metadata import Metadata
from music21.meter import TimeSignature as Music21TimeSignature

from pysong.core.chord import Chord
from pysong.core.interval import Interval
from pysong.core.note import Note
from pysong.core.rest import Rest
from pysong.core.object_with_duration import ObjectWithDuration
from pysong.core.types import NoteLike


class NoteSequence:
    """Sequence of notes/chords/rests. Doesn't necessarily belong to a measure; can represent a sequence without a specific
    start time."""

    def __init__(self, notes: List[ObjectWithDuration] = None):
        if notes:
            for n in notes:
                assert isinstance(n, NoteLike)
            self._notes = notes
        else:
            self._notes = []

    @property
    def notes(self):
        """The list of notes comprising the sequence."""
        return self._notes

    def __iter__(self):
        return self.notes.__iter__()

    def __getitem__(self, key):
        return self.notes[key]

    def __len__(self):
        return len(self.notes)

    def append(self, note: Note):
        """Append the given note to the sequence."""
        self.notes.append(note)

    def append_notes(self, notes: Iterable[Note]):
        """Append the given iterable of Notes to the sequence."""
        for note in notes:
            self.append(note)

    @staticmethod
    def choose_clef(midi_pitch: int) -> Music21Clef:
        if midi_pitch >= 60:
            return TrebleClef()
        else:
            return BassClef()

    def to_music21(self, prev_time_signature=None, prev_clef=None):
        s = stream.Stream()
        # s.append(Metadata(title='pysong fragment', composer='pysong'))

        # Compute an artificial time signature to hold all the notes in the stream.
        numerator = ceil(sum(n.duration.quarters for n in self.notes))

        time_signature = Music21TimeSignature(f'{numerator}/4')
        if (not prev_time_signature or prev_time_signature.numerator != time_signature.numerator
                or prev_time_signature.denominator != time_signature.denominator):
            # print('appending ts', time_signature, prev_time_signature)
            s.append(time_signature)

        clef = None
        for n in self.notes:
            to_append = None
            midi_for_clef = None
            if isinstance(n, Note):
                to_append = Music21Note(n.name_with_octave(fancy_render=False),
                                        duration=Music21Duration(n.duration.quarters))
                midi_for_clef = n.midi_pitch
            elif isinstance(n, Chord):
                # make each music21 note separately and then make chord.
                notes = [Music21Note(n.name_with_octave(fancy_render=False),
                                     duration=Music21Duration(n.duration.quarters))
                         for n in n._notes]
                to_append = Music21Chord(notes)
                midi_for_clef = n._notes[0].midi_pitch
            elif isinstance(n, Rest):
                to_append = Music21Rest(duration=Music21Duration(n.duration.quarters))
            else:
                raise ValueError('Unsupported note type for object', n)

            if midi_for_clef:
                clef = NoteSequence.choose_clef(midi_for_clef)
                if clef != prev_clef:
                    s.append(clef)
                    prev_clef = clef

            s.append(to_append)
        return s, clef

    def show(self):
        """Render using music21."""
        s, clef = self.to_music21()
        print(self)
        s.show()

    def to_intervals(self) -> List[Interval]:
        """Returns a list of intervals between successive notes in the note sequence. For a sequence of length n,
        a list of length n-1 is returned."""
        if len(self) < 2:
            return []

        intervals = []
        prev = self.notes[0]
        for note in self.notes[1:]:
            if isinstance(note, Rest):
                continue
            if isinstance(prev, Chord):
                prev = prev.notes[0]
            intervals.append(Interval.from_notes(prev, note))
            prev = note

        return intervals

    def __repr__(self):
        return ' '.join(n.name_with_octave() for n in self.notes)

""""Represent a sequence of notes. Used in a measure or an unmeasured motif, etc."""

from math import ceil
from typing import Iterable, List

from music21 import stream
from music21.note import Note as Music21Note
from music21.duration import Duration as Music21Duration
from music21.metadata import Metadata
from music21.meter import TimeSignature as Music21TimeSignature

from pysong.core.interval import Interval
from pysong.core.measure import Measure


class MeasureSequence:
    """Sequence of measures. Doesn't necessarily belong to a piece/part; can represent a sequence of measures without a
    specific start time."""

    def __init__(self, measures: List[Measure] = None):
        self._measures = measures

        if not self._measures:
            self._measures = []

    @property
    def measures(self):
        """The list of notes comprising the sequence."""
        return self._measures

    def __iter__(self):
        return self.measures.__iter__()

    def __getitem__(self, key):
        return self.measures[key]

    def __len__(self):
        return len(self.measures)

    def append(self, measure: Measure):
        """Append the given measure to the sequence."""
        assert isinstance(measure, Measure), 'Must append an instance of Measure'
        self.measures.append(measure)

    def append_measures(self, measures: Iterable[Measure]):
        """Append the given iterable of Measures to the sequence."""
        assert isinstance(measures, Iterable)

        for measure in measures:
            self.append(measure)

    def show(self, single_mesasure=False, time_signature=Music21TimeSignature('4/4')):
        """Render using music21."""
        s = stream.Stream()
        s.append(Metadata(title='pysong fragment', composer='pysong'))

        if single_mesasure:
            # Compute an artificial time signature to hold all the notes in the stream.
            numerator = ceil(sum(m.duration for m in self.measures))
            time_signature = Music21TimeSignature(f'{numerator}/4')
            raise NotImplemented

        s.append(time_signature)

        # add all measures.
        clef = None
        for m in self.measures:
            # print('tssss', ts)
            s2, clef = m.notes.to_music21(prev_time_signature=time_signature, prev_clef=clef)
            for x in s2:
                s.append(x)

        # for m in self.measures:
        #     for n in m.notes:
        #         s.append(
        #             Music21Note(n.name_with_octave(fancy_render=False), duration=Music21Duration(n.duration.quarters)))
        print(self)
        s.show()

    def to_intervals(self) -> List[Interval]:
        """Returns a list of intervals between successive notes in the note sequence. For a sequence of length n,
        a list of length n-1 is returned."""
        if len(self) < 1:
            return []

        if len(self.measures[0]) < 2:
            return []

        intervals = []
        prev = None
        for measure in self.measures:
            for note in measure:
                if not prev:
                    prev = note
                    continue
                intervals.append(Interval.from_notes(prev, note))
                prev = note

        return intervals

    def __repr__(self):
        measure_strs = []
        for m in self.measures:
            measure_strs.append(' '.join(n.name_with_octave() for n in m.notes))

        return '\n'.join(measure_strs)

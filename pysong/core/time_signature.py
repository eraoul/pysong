"""Represent a musical time signature."""


class TimeSignature:
    """Represents a time signature."""

    def __init__(self, numerator=4, denominator=4):
        self.numerator = numerator
        self.denominator = denominator

    def __repr__(self):
        return '%d/%d' % (self.numerator, self.denominator)

    def __eq__(self, ts2):
        return (isinstance(ts2, TimeSignature) and self.numerator == ts2.numerator
                and self.denominator == ts2.denominator)

    def __ne__(self, ts2):
        return not self == ts2

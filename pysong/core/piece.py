"""Represent an entire piece composed of multiple instrument parts."""


class Piece:
    """Represents an entire piece of music, composed of multiple parts."""

    def __init__(self):
        pass

    def __repr__(self):
        return 'Piece'

    def show(self):
        """Render via music21."""
        raise NotImplementedError

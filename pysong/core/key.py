"""Represents the key of a musical segment.
"""

from enum import Enum

from pysong.exceptions import ArgumentError


class Mode(Enum):
    """Enum to represent Major/Minor mode."""
    MAJOR = 0
    MINOR = 1


# Lookup tables mapping from pitch class (0-11) to string name of key.
_PC_TO_NAME_MAJOR = ('C', 'Db', 'D', 'Eb', 'E', 'F', 'Gb', 'G', 'Ab', 'A', 'Bb', 'B')
_PC_TO_NAME_MINOR = ('C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'Bb', 'B')

_PC_TO_NAME_SHARP = ('C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B')
_PC_TO_NAME_FLAT = ('C', 'Db', 'D', 'Eb', 'E', 'F', 'Gb', 'G', 'Ab', 'A', 'Bb', 'B')


# Lookup tables mapping from # sharps to pitch class and vice versa.
_SHARPS_TO_TONIC_MAJOR = {i: (7 * i) % 12 for i in range(-6, 7)}
_SHARPS_TO_TONIC_MINOR = {i: (7 * i - 3) % 12 for i in range(-6, 7)}
_TONIC_TO_SHARPS_MAJOR = {_SHARPS_TO_TONIC_MAJOR[i]: i for i in range(-6, 7)}
_TONIC_TO_SHARPS_MINOR = {_SHARPS_TO_TONIC_MINOR[i]: i for i in range(-6, 7)}


class Key:
    """Represents a musical key signature."""

    def __init__(self, tonic_pitch_class: int = None, num_sharps: int = None, mode: Mode = Mode.MAJOR):
        """Must specify either tonic_pitch_class or num_sharps.
        tonic_pitch_class: 0-11. 0=C, 1=C#, etc.
        num_sharps: positive number for # sharps. Negative number represents # of flats.
        mode: Mode enum. MAJOR or MINOR.

         # TODO: using a pitch class for tonic doesn't work for nonstandard notation including Bach's WTC C# Major
         # pieces (such as BWV 848). https://www.bachvereniging.nl/en/bwv/bwv-848/
         # For these we need to specify the tonic note and accidental directly.
        """
        if ((tonic_pitch_class is None and num_sharps is None)
                or (tonic_pitch_class is not None and num_sharps is not None)):
            raise ArgumentError('Specify exactly one of {tonic_pitch_class, num_sharps}')

        assert isinstance(mode, Mode)
        self._mode = mode

        if tonic_pitch_class is not None:
            assert 0 <= tonic_pitch_class < 12
            self._tonic_pitch_class = tonic_pitch_class
        else:
            assert isinstance(num_sharps, int)
            assert -7 < num_sharps < 7
            if mode == Mode.MAJOR:
                self._tonic_pitch_class = _SHARPS_TO_TONIC_MAJOR[num_sharps]
            else:
                self._tonic_pitch_class = _SHARPS_TO_TONIC_MINOR[num_sharps]

    @property
    def mode(self) -> Mode:
        """Returns the mode."""
        return self._mode

    @property
    def tonic_pitch_class(self) -> int:
        """Returns the pitch class of the tonic; e.g. 0 for C, 1 for C#/Db, 2 for D, etc."""
        return self._tonic_pitch_class

    def __repr__(self):
        if self.mode == Mode.MAJOR:
            name_map = _PC_TO_NAME_MAJOR
        else:
            name_map = _PC_TO_NAME_MINOR
        return '%s %s' % (name_map[self.tonic_pitch_class], self.mode.name)

    def __eq__(self, key2):
        return isinstance(key2, Key) and self.tonic_pitch_class == key2.tonic_pitch_class and self.mode == key2.mode

    def is_major(self) -> bool:
        return self.mode == Mode.MAJOR

    @property
    def number_of_sharps(self) -> int:
        """Returns the number of sharps in the key signature, or negative numbers for the number
        of flats, as in MIDI key signatures."""
        if self.is_major:
            return _TONIC_TO_SHARPS_MAJOR[self.tonic_pitch_class]
        return _TONIC_TO_SHARPS_MINOR[self.tonic_pitch_class]

    def pc_to_note_name(self, pc: int) -> str:
        """Maps a standard pitchclass (0-12) to the note name, taking into account the key and mode.

        Bascially if this is a key with sharps, use a sharp for black keys. 

        TODO: UNLESS it's likely to be a flat member in context, then use a flat. Or vice versa. Also
        use double sharps or double flats if needed."""
        if not (0 <= pc < 12):
            raise ArgumentError('pitch class must be 0-11')

        use_sharps = self.number_of_sharps > 0

        if use_sharps:
            return _PC_TO_NAME_SHARP[pc]
        return _PC_TO_NAME_FLAT[pc]

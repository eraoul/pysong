"""Represent a musical entity with a duration. Used as a base class."""

from __future__ import annotations

from typing import Optional

from pysong.exceptions import ArgumentError
from pysong.core.duration import Duration, Quarter


class ObjectWithDuration:
    """Stores a musical entity with a duration. Used as a base class. Ties are optional; a chord can be tied to
    the following note. But a chord can also be specified with a duration that crosses a bar line,
    so ties aren't needed in some applications (such as monophonic lines).

    duration is a float in quarters.
    """

    def __init__(self,
                 duration=Quarter,
                 tie_to_next=False,
                 start_time_quarters: float = None):
        assert isinstance(duration, Duration)
        assert isinstance(tie_to_next, bool)
        if start_time_quarters is not None:
            assert isinstance(start_time_quarters, float)

        # print('setting obj duration', duration)
        self._duration = duration
        self._tie_to_next = tie_to_next
        self._start_time_quarters = start_time_quarters

    # @classmethod
    # def from_obj_with_start_time(cls, note: Note, start_time_quarters: float) -> Note:
    #     """Construct a new Note object with a known start time from an existing note.
    #     Start time is relative to the start of the score (e.g., absolute time).."""
    #     return cls(note_name=note.note_name,
    #                accidental=note.accidental,
    #                octave=note.octave,
    #                duration=note.duration,
    #                velocity=note.velocity,
    #                tie_to_next=note.tie_to_next,
    #                start_time_quarters=start_time_quarters)

    @property
    def duration(self) -> Duration:
        """Returns the duration of the note as a Duration instance."""
        return self._duration

    # setter for duration
    @duration.setter
    def duration(self, duration: Duration):
        assert isinstance(duration, Duration)
        self._duration = duration

    @property
    def tie_to_next(self) -> bool:
        """Returns True if the note is tied to teh next note."""
        return self._tie_to_next

    @property
    def start_time_quarters(self) -> Optional[float]:
        """Returns the absolute start time of this note (relative to the start of the score), or None if not known."""
        return self._start_time_quarters

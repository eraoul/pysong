"""Represent a musical note, including accidentals."""

from __future__ import annotations

from typing import List, Optional

from pysong.exceptions import ArgumentError
from pysong.core.duration import Duration, Quarter
from pysong.core.object_with_duration import ObjectWithDuration
from pysong.core.note import Note


class Rest(ObjectWithDuration):
    """Stores a Rest with a duration.

    duration is a float in quarters.
    """

    def __init__(self,
                 duration=Quarter,
                 start_time_quarters: float = None):
        # initialize base class
        super().__init__(duration, tie_to_next=False, start_time_quarters=start_time_quarters)

    @classmethod
    def from_rest_with_start_time(cls, rest: Rest, start_time_quarters: float) -> Note:
        """Construct a new Rest object with a known start time from an existing rest.
        Start time is relative to the start of the score (e.g., absolute time).."""
        return cls(duration=rest.duration,
                   start_time_quarters=start_time_quarters)

    def __eq__(self, n2):
        return (isinstance(n2, Rest) and self.duration == n2.duration)

    def name(self):
        """Return the rest as a string."""
        return f'rest({self.duration})'

    def __repr__(self) -> str:
        return self.name()

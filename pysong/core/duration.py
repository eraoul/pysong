"""duration.py
"""

from functools import total_ordering
import math


@total_ordering
class Duration:
    """Represents the duration of a musical event as an exact fraction."""
    duration_names = {
        (1, 1): 'whole',
        (1, 2): 'half',
        (1, 4): 'quarter',
        (1, 8): 'eighth',
        (1, 16): 'sixteenth',
        (3, 2): 'dotted-whole',
        (3, 4): 'dotted-half',
        (3, 8): 'dotted-quarter',
        (3, 16): 'dotted-eighth',
    }

    def __init__(self, numerator: int, denominator: int):
        assert isinstance(numerator, int)
        assert isinstance(denominator, int)
        assert numerator >= 0
        assert denominator > 0

        # reduce to lowest terms fraction
        gcd = math.gcd(numerator, denominator)
        self._numerator = numerator // gcd
        self._denominator = denominator // gcd

    @property
    def numerator(self) -> int:
        """Numerator of the duration fraction; e.g. 1 for a quarter note (1/4)"""
        return self._numerator

    @property
    def denominator(self) -> int:
        """Denominator of the duration fraction; e.g. 4 for a quarter note (1/4)"""
        return self._denominator

    @property
    def quarters(self) -> float:
        """Length in quarter notes; e.g. 0.5 for an eighth note."""
        return self.numerator / (self.denominator / 4.0)

    def __eq__(self, d2):
        return isinstance(d2, Duration) and self.numerator == d2.numerator and self.denominator == d2.denominator

    def __lt__(self, d2):
        assert isinstance(d2, Duration)
        return self.quarters < d2.quarters

    def __repr__(self):
        return self.duration_names.get((self.numerator, self.denominator), f'{self.numerator}/{self.denominator}')


Quarter = Duration(1, 4)
Half = Duration(1, 2)
Whole = Duration(1, 1)
Eighth = Duration(1, 8)
Sixteenth = Duration(1, 16)
DottedWhole = Duration(3, 2)
DottedHalf = Duration(3, 4)
DottedQuarter = Duration(3, 8)
DottedEighth = Duration(3, 16)

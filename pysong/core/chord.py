"""Represent a musical note, including accidentals."""

from __future__ import annotations

from typing import List, Optional

from pysong.exceptions import ArgumentError
from pysong.core.duration import Duration, Quarter
from pysong.core.object_with_duration import ObjectWithDuration
from pysong.core.note import Note


class Chord(ObjectWithDuration):
    """Stores a Chord with a duration. Ties are optional; a chord can be tied to
    the following chord. But a note can also be specified with a duration that crosses a bar line,
    so ties aren't needed in some applications (such as monophonic lines).

    All notes in a chord must be of the same duration. To represent variable duration with
    notes starting at the same time, use multiple voices instead.

    duration is a float in quarters.
    """

    def __init__(self,
                 notes: List[Note] = None,
                 duration=Quarter,
                 tie_to_next=False,
                 start_time_quarters: float = None):
        # initialize base class
        # print('\nCHORD init')
        super().__init__(duration, tie_to_next=tie_to_next, start_time_quarters=start_time_quarters)

        if notes is not None:
            assert isinstance(notes, List)
            assert len(notes) > 0
            assert isinstance(notes[0], Note)

            self._notes = notes
            for note in self._notes:
                note.duration = duration
        else:
            self._notes = []

    def __iter__(self):
        return self._notes.__iter__()

    @classmethod
    def from_chord_with_start_time(cls, chord: Chord, start_time_quarters: float) -> Note:
        """Construct a new Chord object with a known start time from an existing chord.
        Start time is relative to the start of the score (e.g., absolute time).."""
        return cls(notes=chord.notes,
                   duration=chord.duration,
                   tie_to_next=chord.tie_to_next,
                   start_time_quarters=start_time_quarters)

    def has_same_notes(self, c2: Chord) -> bool:
        if len(self._notes) != len(c2.notes):
            return False
        for n1, n2 in zip(self.notes, c2.notes):
            if n1 != n2:
                return False
        return True

    def __eq__(self, c2):
        return (isinstance(c2, Chord) and self._notes == c2.notes and self.has_same_notes(c2.notes)
                and self.duration == c2.duration and self.tie_to_next == c2.tie_to_next)

    def name(self, fancy_render: bool = True):
        """Return the note names and accidentals as a string. Use unicode symbols if fancy_render==True."""
        note_names = [n.name(fancy_render=fancy_render) for n in self._notes]
        return ' '.join(note_names)

    def name_with_octave(self, fancy_render: bool = True):
        """Return the note names+octaves and accidentals as a string. Use unicode symbols if fancy_render==True."""
        note_names = [n.name_with_octave(fancy_render=fancy_render) for n in self._notes]
        return f'[{' '.join(note_names)}]'

    def __repr__(self):
        tie_string = ''

        if self.tie_to_next:
            tie_string = '[TIE]'

        return f'{self.name_with_octave()} {self.duration}{tie_string}'

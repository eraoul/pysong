
from pysong.core.chord import Chord
from pysong.core.note import Note
from pysong.core.rest import Rest

NoteLike = Note or Chord or Rest

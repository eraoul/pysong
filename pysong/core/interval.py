"""Represent the musical interval between two notes in tonal harmony."""

from __future__ import annotations

from collections import defaultdict
from enum import Enum
from functools import total_ordering

from pysong.exceptions import ArgumentError, ExtendedIntervalError
from pysong.core.note import Note


class IntervalDirection(Enum):
    """Enum to represent interval direction. Use NONE for simultaneous notes, or ascending or descending for notes
    separated in time."""
    NONE = 0
    ASCENDING = 1
    DESCENDING = 2


class IntervalQuality(Enum):
    """Enum to represent interval quality."""
    PERFECT = 0
    MAJOR = 1
    MINOR = 2
    DIM = 3
    AUG = 4


_PERFECT_SIZE_TO_HALFSTEPS = {
    0: 0,  # unison
    3: 5,  # perfect 4th
    4: 7
}  # perfect 5th

_MAJOR_SIZE_TO_HALFSTEPS = {
    1: 2,  # major 2nd
    2: 4,  # major 3rd
    5: 9,  # major 6th
    6: 11
}  # major 7th

_MINOR_SIZE_TO_HALFSTEPS = {
    1: 1,  # minor 2nd
    2: 3,  # minor 3rd
    5: 8,  # minor 6th
    6: 10
}  # minor 7th

_DIM_SIZE_TO_HALFSTEPS = {
    0: -1,  # dim unison
    1: 0,  # dim 2nd
    2: 2,  # dim 3rd
    3: 4,  # dim 4th
    4: 6,  # the devil (tritone, dim 5th)
    5: 7,  # dim 6th
    6: 9
}  # dim 7th

_AUG_SIZE_TO_HALFSTEPS = {
    0: 1,  # aug unison
    1: 3,  # aug 2nd
    2: 5,  # aug 3rd
    3: 6,  # the devil  (tritone, aug 4th)
    4: 8,  # aug 5th
    5: 10,  # aug 6th
    6: 12
}  # aug 7th

# Map quality to a size->halfstep dict.
_QUALITY_TO_HALFSTEP_DICT = {
    IntervalQuality.MAJOR: _MAJOR_SIZE_TO_HALFSTEPS,
    IntervalQuality.MINOR: _MINOR_SIZE_TO_HALFSTEPS,
    IntervalQuality.PERFECT: _PERFECT_SIZE_TO_HALFSTEPS,
    IntervalQuality.DIM: _DIM_SIZE_TO_HALFSTEPS,
    IntervalQuality.AUG: _AUG_SIZE_TO_HALFSTEPS
}


def _compute_size_to_allowed_qualities():
    d = defaultdict(set)

    for quality, size_dict in _QUALITY_TO_HALFSTEP_DICT.items():
        for size in size_dict:
            d[size].add(quality)
    return d


# Map size in half steps to allowed interval qualities (e.g., 6 half steps can be DIM or AUG, for 4th or 5th)
_SIZE_TO_ALLOWED_QUALITIES = _compute_size_to_allowed_qualities()

_SIZE_STRS = ('unison', '2nd', '3rd', '4th', '5th', '6th', '7th', 'octave')

# String suffixes such as 20th, 21st, 22nd, etc.
_NUMBER_SUFFIXES = ('th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th')
_TEEN_NUMBER_SUFFIX = 'th'


@total_ordering
class Interval:
    """Represents the tonal interval between two notes, such as "perfect 4th".
    """

    def __init__(self, quality: IntervalQuality, size: int, direction: IntervalDirection = IntervalDirection.NONE):
        """size is the distance between 2 notes in the interval, in note-names. E.g. the 3rd from D to F is size 2."""
        if not isinstance(quality, IntervalQuality):
            raise ArgumentError(f'{quality} must be of type IntervalQuality')
        if not isinstance(size, int):
            raise ArgumentError(f'{size} must be of type int')
        if not size >= 0:
            raise ArgumentError(f'size ({size}) must be >= 0')
        if not isinstance(direction, IntervalDirection):
            raise ArgumentError(f'{direction} must be of type IntervalDirection')

        # Make sure we don't have a "minor" unison, 4th, 5th, or octave; those aren't allowed.
        self._mod_size = size % 7
        if quality not in _SIZE_TO_ALLOWED_QUALITIES[self._mod_size]:
            raise ArgumentError(f'{quality} not allowed for interval size {size} [{self._mod_size}]')

        self._quality = quality
        self._size = size
        self._direction = direction

    @property
    def quality(self) -> IntervalQuality:
        """Returns the interval quality."""
        return self._quality

    @property
    def size(self) -> int:
        """Returns the interval size (distance between white-key note names). E.g. C-E has size 2."""
        return self._size

    @property
    def direction(self) -> IntervalDirection:
        """Returns the direction of the interval, or NONE for simultaneous notes."""
        return self._direction

    @property
    def octaves(self) -> int:
        """Returns the number of octaves spanned by the interval. E.g. 0 for a 7th, 1 for a 9th, etc."""
        return self.size // 7

    def _half_step_offset(self) -> int:
        return _QUALITY_TO_HALFSTEP_DICT[self.quality][self._mod_size]

    @property
    def half_steps(self) -> int:
        """Compute # half-steps in the interval."""
        return self.octaves * 12 + self._half_step_offset()

    def __lt__(self, i2):
        assert isinstance(i2, Interval)
        if self._size < i2._size:
            return True
        if self._size > i2._size:
            return False

        return self._half_step_offset() < i2._half_step_offset()

    def __eq__(self, i2):
        assert isinstance(i2, Interval)
        return self._size == i2._size and self.quality == i2.quality

    @classmethod
    def from_notes(cls, note1: Note, note2: Note) -> Interval:
        """Returns a new interval based on the difference from the lower note to the higher note. Notes are sorted
        automatically."""

        # Sort so that note 1 <= note 2.
        if note1 > note2:
            note1, note2 = note2, note1

        half_steps = note2.midi_pitch - note1.midi_pitch
        size = note2.pc_including_octave - note1.pc_including_octave
        mod_size = size % 7

        possible_qualities = _SIZE_TO_ALLOWED_QUALITIES[mod_size]

        # TODO: optimize. This just computes interval by trial-and-error until getting the right interval in half-steps.
        # Also, make this handle arbitrary numbers of augs or dims (double-augmented, etc) to handle modern music or
        # bad notation.
        for q in possible_qualities:
            interval = cls(q, size)
            h = interval.half_steps
            if h == half_steps:
                return interval
        raise ExtendedIntervalError(f'Logic Error computing interval between {note1} and {note2}')

    @property
    def quality_str(self) -> str:
        """Returns a string describing the quality of the interval ("perfect", "major", etc.)"""
        return self.quality.name.lower()

    @staticmethod
    def _numeric_abbreviation_str(num) -> str:
        """Return an abbreviated for an ordinal number such 10th or 21st."""
        assert num >= 0
        suffix = ''
        if 10 <= num % 100 < 20:
            suffix = _TEEN_NUMBER_SUFFIX
        else:
            suffix = _NUMBER_SUFFIXES[num]
        return f'{num}{suffix}'

    @property
    def size_str(self) -> str:
        """Returns the interval size as a string."""
        if self._size < len(_SIZE_STRS):
            return _SIZE_STRS[self._size]
        return self._numeric_abbreviation_str(self._size + 1)  # e.g. 10th or 21st

    def __repr__(self):
        # Say "octave" or "unison" instead of "perfect octave" or "perfect unison"
        if (self._size == 0 or self._size == 7) and (self.quality == IntervalQuality.PERFECT):
            return self.size_str

        return f'{self.quality_str} {self.size_str}'

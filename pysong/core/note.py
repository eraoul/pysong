"""Represent a musical note, including accidentals."""

from __future__ import annotations

from enum import Enum
from functools import total_ordering
from typing import Optional

from pysong.core.key import Key
from pysong.exceptions import ArgumentError
from pysong.core.duration import Duration, Quarter
from pysong.core.object_with_duration import ObjectWithDuration

_UNICODE_DOUBLE_FLAT = '\U0001D12B'
_UNICODE_FLAT = '\u266D'
_UNICODE_NATURAL = '\u266E'
_UNICODE_SHARP = '\u266F'
_UNICODE_DOUBLE_SHARP = '\U0001D12A'


@total_ordering
class Accidental(Enum):
    """Enum giving possible note accidentals."""
    DoubleFlat = -2
    Flat = -1
    Natural = 0
    Sharp = 1
    DoubleSharp = 2

    def __lt__(self, a2):
        assert isinstance(a2, Accidental)
        return self.value < a2.value  # pylint: disable=comparison-with-callable


_ACCIDENTAL_TO_UNICODE = {
    Accidental.DoubleFlat: _UNICODE_DOUBLE_FLAT,
    Accidental.Flat: _UNICODE_FLAT,
    Accidental.Natural: _UNICODE_NATURAL,
    Accidental.Sharp: _UNICODE_SHARP,
    Accidental.DoubleSharp: _UNICODE_DOUBLE_SHARP
}

_ACCIDENTAL_TO_STRING = {
    Accidental.DoubleFlat: 'bb',
    Accidental.Flat: 'b',
    Accidental.Natural: 'n',
    Accidental.Sharp: '#',
    Accidental.DoubleSharp: 'x'
}

_STRING_TO_ACCIDENTAL = {v: k for k, v in _ACCIDENTAL_TO_STRING.items()}

_NOTE_NAME_TO_PC = {'C': 0, 'D': 2, 'E': 4, 'F': 5, 'G': 7, 'A': 9, 'B': 11}
_NOTE_NAMES = frozenset(_NOTE_NAME_TO_PC.keys())

_NOTE_NAME_TO_WHITEKEY = {'C': 0, 'D': 1, 'E': 2, 'F': 3, 'G': 4, 'A': 5, 'B': 6}


@total_ordering
class Note(ObjectWithDuration):
    """Stores a single Note with a duration. Ties are optional; a note can be tied to
    the following note. But a note can also be specified with a duration that crosses a bar line,
    so ties aren't needed in some applications (such as monophonic lines).

    duration is a float in quarters.
    """

    def __init__(self,
                 note_name='C',
                 accidental=Accidental.Natural,
                 octave=4,
                 duration=Quarter,
                 velocity=None,
                 tie_to_next=False,
                 start_time_quarters: float = None):
        # initialize base class
        # print('note init')

        super().__init__(duration, tie_to_next=tie_to_next, start_time_quarters=start_time_quarters)

        assert isinstance(note_name, str), f'note_name must be a string, not {type(note_name)}'
        assert isinstance(accidental, Accidental)
        assert isinstance(octave, int)
        assert -1 <= octave <= 9
        if velocity:
            assert isinstance(velocity, int)
            assert 0 <= velocity < 128

        note_name = note_name.upper()
        if note_name not in _NOTE_NAMES:
            raise ArgumentError(f'Unknown note name {note_name}')
        self._note_name = note_name
        self._accidental = accidental
        self._octave = octave
        self._velocity = velocity  # Optional. From 0-127, as in MIDI. Set to None for a default velocity.

    @classmethod
    def from_note_with_start_time(cls, note: Note, start_time_quarters: float) -> Note:
        """Construct a new Note object with a known start time from an existing note.
        Start time is relative to the start of the score (e.g., absolute time).."""
        return cls(note_name=note.note_name,
                   accidental=note.accidental,
                   octave=note.octave,
                   duration=note.duration,
                   velocity=note.velocity,
                   tie_to_next=note.tie_to_next,
                   start_time_quarters=start_time_quarters)

    @property
    def note_name(self) -> str:
        """Returns the string name of the note without accidental ('C', 'D', etc.)."""
        return self._note_name

    @property
    def accidental(self) -> Accidental:
        """Returns the Accidental of the note (always defined, even if in the key or natural)."""
        return self._accidental

    @property
    def octave(self) -> int:
        """Returns the octave of the note."""
        return self._octave

    # octave setter
    @octave.setter
    def octave(self, value):
        self._octave = value

    @property
    def velocity(self) -> int:
        """Returns the velocity of the note."""
        return self._velocity

    def __eq__(self, n2):
        return (isinstance(n2, Note) and self.note_name == n2.note_name and self.accidental == n2.accidental
                and self.octave == n2.octave and self.duration == n2.duration and self.velocity == n2.velocity
                and self.tie_to_next == n2.tie_to_next)

    @property
    def pc(self) -> int:
        """"Returns the standard MIDI pitch of this note."""
        return (self._NOTE_NAME_TO_PC[self.note_name] + self.accidental.value) % 12

    @property
    def midi_pitch(self) -> int:
        """"Returns the standard MIDI pitch of this note."""
        return (self.octave + 1) * 12 + _NOTE_NAME_TO_PC[self.note_name] + self.accidental.value

    @property
    def pc_including_octave(self) -> int:
        """Computes the note_number for the pc of this note, where there are 7 pcs per octave instead of 12.
        Accidentals are ignored.

        e.g. midi C4 is mapped to 7*4 = 28. D4 is 29. C5 is 35.
        C#4 is also 28, and Db4 is 29.
        """
        return self.octave * 7 + _NOTE_NAME_TO_WHITEKEY[self.note_name]

    def accidental_str(self, display_natural: bool = False, fancy_render: bool = True) -> str:
        """Return the accidental as a string. Use unicode symbols if fancy_render==True."""
        if not display_natural and self.accidental == Accidental.Natural:
            return ''
        if fancy_render:
            return _ACCIDENTAL_TO_UNICODE[self.accidental]
        return _ACCIDENTAL_TO_STRING[self.accidental]

    def name(self, fancy_render: bool = True):
        """Return the note name and accidental as a string. Use unicode symbols if fancy_render==True."""
        return f'{self.note_name}{self.accidental_str(fancy_render=fancy_render)}'

    def name_with_octave(self, fancy_render: bool = True):
        """Return the note name and octave as a string. Use unicode symbols if fancy_render==True."""
        return f'{self.name(fancy_render=fancy_render)}{self.octave}'

    def __lt__(self, n2: Note):
        """Ordering is based on pitch (note name, then accidental. Duration etc is ignored."""
        assert isinstance(n2, Note)

        n1_pc = self.pc_including_octave
        n2_pc = n2.pc_including_octave

        if n1_pc < n2_pc:
            return True

        if n1_pc > n2_pc:
            return False

        return self.accidental < n2.accidental

    def __repr__(self):
        velocity_string = None
        tie_string = None

        if self.velocity and self.velocity > 0:
            velocity_string = f'v={self.velocity}'
        if self.tie_to_next:
            tie_string = 'TIE'
        extra = ' '.join(filter(lambda x: x is not None, [velocity_string, tie_string]))
        if extra:
            extra = f'[{extra}]'

        return f'{self.name_with_octave()} {self.duration}{extra}'

    @staticmethod
    def midi_to_note_name(midi_pitch: int, key: Key = None) -> str:
        """Maps a standard MIDI pitch to the note name."""
        return Note.pc_to_note_name(midi_pitch % 12, key)

    @staticmethod
    def pc_to_note_name(pc: int, key: Key = None) -> str:
        """Maps a standard MIDI pitch to the note name."""
        if not key:
            key = Key(num_sharps=0)
        return key.pc_to_note_name(pc)

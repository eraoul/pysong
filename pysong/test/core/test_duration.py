import unittest

from pysong.core.duration import Duration, Quarter, Half, Whole, Eighth, Sixteenth


class DurationTest(unittest.TestCase):

    def test_duration_str(self):
        d = Duration(1, 4)
        self.assertEqual('quarter', str(d))

        self.assertEqual('quarter', str(Quarter))
        self.assertEqual('half', str(Half))
        self.assertEqual('whole', str(Whole))
        self.assertEqual('eighth', str(Eighth))
        self.assertEqual('sixteenth', str(Sixteenth))

        d = Duration(3, 4)
        self.assertEqual('dotted-half', str(d))

        d = Duration(5, 4)
        self.assertEqual('5/4', str(d))

        d = Duration(3, 32)
        self.assertEqual('3/32', str(d))

        d = Duration(1, 1)
        self.assertEqual('whole', str(d))

        d = Duration(4, 4)
        self.assertEqual('whole', str(d))

        d = Duration(2, 4)
        self.assertEqual('half', str(d))

        d = Duration(6, 16)
        self.assertEqual('dotted-quarter', str(d))


if __name__ == '__main__':
    unittest.main()

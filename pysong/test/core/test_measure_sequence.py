import unittest

from pysong.core import Accidental, Measure, MeasureSequence, NoteSequence, Note

C = Note('C')
F = Note('F')
G = Note('G')
C5_SHARP = Note('C', Accidental.Sharp, octave=5)


class MeasureSequenceTest(unittest.TestCase):

    def test_create(self):
        s = NoteSequence([C, F, G])
        m = Measure(notes=s)
        ms = MeasureSequence([m])
        self.assertEqual(1, len(ms))

    def test_append(self):
        s = NoteSequence([C, F, G])
        m = Measure(notes=s)
        ms = MeasureSequence([m])

        s2 = NoteSequence([G, F, C5_SHARP])
        m2 = Measure(notes=s2)

        ms.append(m2)

        self.assertEqual(2, len(ms))

    def test_append_measures(self):
        s = NoteSequence([C, F, G])
        m = Measure(notes=s)
        s2 = NoteSequence([G, F, C5_SHARP])
        m2 = Measure(notes=s2)

        ms = MeasureSequence()
        ms.append_measures([m, m2])

        self.assertEqual(2, len(ms))

    def test_to_intervals_empty(self):
        ms = MeasureSequence()
        intervals = ms.to_intervals()
        self.assertEqual(0, len(intervals))

    def test_to_intervals_single_measure(self):
        s = NoteSequence([C, F, G])
        m = Measure(notes=s)
        ms = MeasureSequence([m])

        intervals = ms.to_intervals()
        self.assertEqual(2, len(intervals))

        self.assertEqual('perfect 4th', str(intervals[0]))
        self.assertEqual('major 2nd', str(intervals[1]))

    def test_to_intervals_two_measures(self):
        s = NoteSequence([C, F, G])
        m = Measure(notes=s)
        s2 = NoteSequence([C5_SHARP, F])
        m2 = Measure(notes=s2)

        ms = MeasureSequence()
        ms.append_measures([m, m2])

        intervals = ms.to_intervals()
        self.assertEqual(4, len(intervals))

        self.assertEqual('perfect 4th', str(intervals[0]))
        self.assertEqual('major 2nd', str(intervals[1]))
        self.assertEqual('aug 4th', str(intervals[2]))
        self.assertEqual('aug 5th', str(intervals[3]))


if __name__ == '__main__':
    unittest.main()

import unittest

from pysong.core.note import Accidental, Note
from pysong.core.duration import Duration, Quarter, Half, Whole, Eighth, Sixteenth


class NoteTest(unittest.TestCase):

    def test_ordering(self):
        n1 = Note('C', Accidental.Flat)
        n2 = Note('C', Accidental.Flat)
        self.assertEqual(n1, n2)

        n1 = Note('C', Accidental.Natural)
        n2 = Note('C', Accidental.Sharp)
        self.assertLess(n1, n2)

        n1 = Note('C', Accidental.Sharp)
        n2 = Note('D', Accidental.Flat)
        self.assertLess(n1, n2)

        n1 = Note('C', Accidental.DoubleSharp)
        n2 = Note('D', Accidental.DoubleFlat)
        self.assertLess(n1, n2)

    def test_note_name(self):
        n = Note()
        self.assertEqual('C4', n.name_with_octave(fancy_render=False))

        n = Note('B', Accidental.Sharp, 1)
        self.assertEqual('B#1', n.name_with_octave(fancy_render=False))

        n = Note('F', Accidental.Flat, 2)
        self.assertEqual('Fb2', n.name_with_octave(fancy_render=False))

        n = Note('F', Accidental.DoubleSharp, 5)
        self.assertEqual('Fx5', n.name_with_octave(fancy_render=False))

        n = Note('C', Accidental.DoubleFlat, 3)
        self.assertEqual('Cbb3', n.name_with_octave(fancy_render=False))

    def test_note_str_fancy(self):
        n = Note()
        self.assertEqual('C4 quarter', str(n))

        n = Note(duration=Quarter)
        self.assertEqual('C4 quarter', str(n))

        n = Note(duration=Duration(3, 8))
        self.assertEqual('C4 dotted-quarter', str(n))

        n = Note('B', Accidental.Sharp, 1, Eighth)
        self.assertEqual('B♯1 eighth', str(n))

        n = Note('F', Accidental.Flat, 2, Whole)
        self.assertEqual('F♭2 whole', str(n))

        n = Note('F', Accidental.DoubleSharp, 5, Sixteenth)
        self.assertEqual('F𝄪5 sixteenth', str(n))

        n = Note('C', Accidental.DoubleFlat, 3, Half)
        self.assertEqual('C𝄫3 half', str(n))

    def test_note_midi(self):
        n = Note()
        self.assertEqual(60, n.midi_pitch)

        n = Note('B', Accidental.Sharp, 1)
        self.assertEqual(36, n.midi_pitch)

        n = Note('F', Accidental.Flat, 2)
        self.assertEqual(40, n.midi_pitch)

        n = Note('E', Accidental.DoubleSharp, 5)
        self.assertEqual(78, n.midi_pitch)

        n = Note('C', Accidental.DoubleFlat, 3)
        self.assertEqual(46, n.midi_pitch)

    def test_pc_with_octave(self):
        n = Note()
        self.assertEqual(28, n.pc_including_octave)

        n = Note('F', Accidental.Sharp, 3)
        self.assertEqual(24, n.pc_including_octave)

        n = Note('G', Accidental.Flat, 3)
        self.assertEqual(25, n.pc_including_octave)


if __name__ == '__main__':
    unittest.main()

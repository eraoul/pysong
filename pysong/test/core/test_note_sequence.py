import unittest

from pysong.core.note import Note, Accidental
from pysong.core.note_sequence import NoteSequence

C = Note('C')
F = Note('F')
G = Note('G')
C5_SHARP = Note('C', Accidental.Sharp, octave=5)


class NoteSequenceTest(unittest.TestCase):

    def test_create(self):
        s = NoteSequence([C, F, G])
        self.assertEqual(3, len(s))

    def test_append(self):
        s = NoteSequence()
        s.append(C)
        s.append(F)
        self.assertEqual(2, len(s))

        s = NoteSequence()
        s.append_notes([C, F, G])
        self.assertEqual(3, len(s))

    def test_to_intervals_empty(self):
        s = NoteSequence()
        intervals = s.to_intervals()
        self.assertEqual(0, len(intervals))

    def test_to_intervals_single(self):
        s = NoteSequence([C])
        intervals = s.to_intervals()
        self.assertEqual(0, len(intervals))

    def test_to_intervals(self):
        s = NoteSequence([C, F, G, C5_SHARP])
        intervals = s.to_intervals()

        self.assertEqual(3, len(intervals))

        self.assertEqual('perfect 4th', str(intervals[0]))
        self.assertEqual('major 2nd', str(intervals[1]))
        self.assertEqual('aug 4th', str(intervals[2]))

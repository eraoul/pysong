import unittest

from pysong.core.interval import Interval, IntervalQuality
from pysong.core.note import Note, Accidental
from pysong.exceptions import ArgumentError, ExtendedIntervalError

C_FLAT = Note('C', Accidental.Flat)
C = Note('C', Accidental.Natural)
C_SHARP = Note('C', Accidental.Sharp)
C_DOUBLESHARP = Note('C', Accidental.DoubleSharp)
D_DOUBLEFLAT = Note('D', Accidental.DoubleFlat)
D_FLAT = Note('D', Accidental.Flat)
D = Note('D', Accidental.Natural)
D_SHARP = Note('D', Accidental.Sharp)
D_DOUBLESHARP = Note('D', Accidental.DoubleSharp)
E_DOUBLEFLAT = Note('E', Accidental.DoubleFlat)
E_FLAT = Note('E', Accidental.Flat)
E = Note('E', Accidental.Natural)
F = Note('F', Accidental.Natural)
F_SHARP = Note('F', Accidental.Sharp)
G_FLAT = Note('G', Accidental.Flat)
G = Note('G', Accidental.Natural)
A = Note('A', Accidental.Natural)
B = Note('B', Accidental.Natural)
C5_FLAT = Note('C', Accidental.Flat, 5)
C5 = Note('C', Accidental.Natural, 5)
C5_SHARP = Note('C', Accidental.Sharp, 5)


class IntervalTest(unittest.TestCase):

    def test_ordering(self):
        i1 = Interval(IntervalQuality.MINOR, 1)
        i2 = Interval(IntervalQuality.MAJOR, 1)
        self.assertLess(i1, i2)

        i1 = Interval(IntervalQuality.DIM, 1)
        i2 = Interval(IntervalQuality.MINOR, 1)
        self.assertLess(i1, i2)

        i1 = Interval(IntervalQuality.PERFECT, 3)
        i2 = Interval(IntervalQuality.PERFECT, 4)
        self.assertLess(i1, i2)
        self.assertGreater(i2, i1)

        i1 = Interval(IntervalQuality.AUG, 1)
        i2 = Interval(IntervalQuality.DIM, 2)
        self.assertLess(i1, i2)

        i1 = Interval(IntervalQuality.MINOR, 5)
        i2 = Interval(IntervalQuality.MINOR, 5)
        self.assertEqual(i1, i2)

    def test_from_notes(self):
        i = Interval.from_notes(C, C)
        self.assertEqual('unison', str(i))

        i = Interval.from_notes(C, E)
        self.assertEqual('major 3rd', str(i))

        i = Interval.from_notes(E, C)
        self.assertEqual('major 3rd', str(i))

        i = Interval.from_notes(C, F_SHARP)
        self.assertEqual('aug 4th', str(i))

        i = Interval.from_notes(C, G_FLAT)
        self.assertEqual('dim 5th', str(i))

        i = Interval.from_notes(C, C5)
        self.assertEqual('octave', str(i))

        i = Interval.from_notes(C, C_FLAT)
        self.assertEqual('aug unison', str(i))

        i = Interval.from_notes(C, C5_FLAT)
        self.assertEqual('dim octave', str(i))

        i = Interval.from_notes(C, C5_SHARP)
        self.assertEqual('aug octave', str(i))

        i = Interval.from_notes(C_SHARP, D_DOUBLESHARP)
        self.assertEqual('aug 2nd', str(i))

        i = Interval.from_notes(C_SHARP, D_FLAT)
        self.assertEqual('dim 2nd', str(i))

        i = Interval.from_notes(C_DOUBLESHARP, D)
        self.assertEqual('dim 2nd', str(i))

        i = Interval.from_notes(C_SHARP, E_FLAT)
        self.assertEqual('dim 3rd', str(i))

    def test_from_notes_double_dim(self):
        # Doubly-diminished third not handled.
        self.assertRaises(ExtendedIntervalError, lambda: Interval.from_notes(C_SHARP, E_DOUBLEFLAT))

        # i = Interval.from_notes(C_SHARP, E_DOUBLEFLAT)
        # self.assertEqual('aug unison', str(i))

    def test_from_notes_double_aug(self):
        # Doubly-augmented unison not handled.
        self.assertRaises(ExtendedIntervalError, lambda: Interval.from_notes(D_FLAT, D_SHARP))

        # i = Interval.from_notes(D_FLAT, D_SHARP)
        # self.assertEqual('aug unison', str(i))

        # Triply-augmented unison not handled.
        self.assertRaises(ExtendedIntervalError, lambda: Interval.from_notes(D_FLAT, D_DOUBLESHARP))

        # i = Interval.from_notes(D_FLAT, D_DOUBLESHARP)
        # self.assertEqual('aug-aug-aug unison', str(i))

    def test_illegal_intervals(self):
        self.assertRaises(ArgumentError, lambda: Interval(IntervalQuality.MAJOR, 0))
        self.assertRaises(ArgumentError, lambda: Interval(IntervalQuality.MAJOR, 3))
        self.assertRaises(ArgumentError, lambda: Interval(IntervalQuality.MAJOR, 4))
        self.assertRaises(ArgumentError, lambda: Interval(IntervalQuality.MAJOR, 7))
        self.assertRaises(ArgumentError, lambda: Interval(IntervalQuality.MAJOR, 10))

        self.assertRaises(ArgumentError, lambda: Interval(IntervalQuality.PERFECT, 1))
        self.assertRaises(ArgumentError, lambda: Interval(IntervalQuality.PERFECT, 8))

        self.assertRaises(ArgumentError, lambda: Interval(IntervalQuality.MINOR, 0))
        self.assertRaises(ArgumentError, lambda: Interval(IntervalQuality.MINOR, 3))
        self.assertRaises(ArgumentError, lambda: Interval(IntervalQuality.MINOR, 4))
        self.assertRaises(ArgumentError, lambda: Interval(IntervalQuality.MINOR, 7))
        self.assertRaises(ArgumentError, lambda: Interval(IntervalQuality.MINOR, 10))

    def test_interval_perfect(self):
        i = Interval(IntervalQuality.PERFECT, 0)
        self.assertEqual('unison', str(i))
        self.assertEqual(0, i.half_steps)

        i = Interval(IntervalQuality.PERFECT, 3)
        self.assertEqual('perfect 4th', str(i))
        self.assertEqual(5, i.half_steps)

        i = Interval(IntervalQuality.PERFECT, 4)
        self.assertEqual('perfect 5th', str(i))
        self.assertEqual(7, i.half_steps)

        i = Interval(IntervalQuality.PERFECT, 7)
        self.assertEqual('octave', str(i))
        self.assertEqual(12, i.half_steps)

        i = Interval(IntervalQuality.PERFECT, 10)
        self.assertEqual('perfect 11th', str(i))
        self.assertEqual(17, i.half_steps)

        i = Interval(IntervalQuality.PERFECT, 11)
        self.assertEqual('perfect 12th', str(i))
        self.assertEqual(19, i.half_steps)

    def test_interval_major(self):
        i = Interval(IntervalQuality.MAJOR, 1)
        self.assertEqual('major 2nd', str(i))
        self.assertEqual(2, i.half_steps)

        i = Interval(IntervalQuality.MAJOR, 2)
        self.assertEqual('major 3rd', str(i))
        self.assertEqual(4, i.half_steps)

        i = Interval(IntervalQuality.MAJOR, 5)
        self.assertEqual('major 6th', str(i))
        self.assertEqual(9, i.half_steps)

        i = Interval(IntervalQuality.MAJOR, 6)
        self.assertEqual('major 7th', str(i))
        self.assertEqual(11, i.half_steps)

        i = Interval(IntervalQuality.MAJOR, 8)
        self.assertEqual('major 9th', str(i))
        self.assertEqual(14, i.half_steps)

        i = Interval(IntervalQuality.MAJOR, 9)
        self.assertEqual('major 10th', str(i))
        self.assertEqual(16, i.half_steps)

    def test_interval_minor(self):
        i = Interval(IntervalQuality.MINOR, 1)
        self.assertEqual('minor 2nd', str(i))
        self.assertEqual(1, i.half_steps)

        i = Interval(IntervalQuality.MINOR, 2)
        self.assertEqual('minor 3rd', str(i))
        self.assertEqual(3, i.half_steps)

        i = Interval(IntervalQuality.MINOR, 5)
        self.assertEqual('minor 6th', str(i))
        self.assertEqual(8, i.half_steps)

        i = Interval(IntervalQuality.MINOR, 6)
        self.assertEqual('minor 7th', str(i))
        self.assertEqual(10, i.half_steps)

        i = Interval(IntervalQuality.MINOR, 8)
        self.assertEqual('minor 9th', str(i))
        self.assertEqual(13, i.half_steps)

        i = Interval(IntervalQuality.MINOR, 9)
        self.assertEqual('minor 10th', str(i))
        self.assertEqual(15, i.half_steps)

    def test_interval_dim(self):
        i = Interval(IntervalQuality.DIM, 0)
        self.assertEqual('dim unison', str(i))
        self.assertEqual(-1, i.half_steps)

        i = Interval(IntervalQuality.DIM, 1)
        self.assertEqual('dim 2nd', str(i))
        self.assertEqual(0, i.half_steps)

        i = Interval(IntervalQuality.DIM, 2)
        self.assertEqual('dim 3rd', str(i))
        self.assertEqual(2, i.half_steps)

        i = Interval(IntervalQuality.DIM, 3)
        self.assertEqual('dim 4th', str(i))
        self.assertEqual(4, i.half_steps)

        i = Interval(IntervalQuality.DIM, 4)
        self.assertEqual('dim 5th', str(i))
        self.assertEqual(6, i.half_steps)

        i = Interval(IntervalQuality.DIM, 5)
        self.assertEqual('dim 6th', str(i))
        self.assertEqual(7, i.half_steps)

        i = Interval(IntervalQuality.DIM, 6)
        self.assertEqual('dim 7th', str(i))
        self.assertEqual(9, i.half_steps)

        i = Interval(IntervalQuality.DIM, 7)
        self.assertEqual('dim octave', str(i))
        self.assertEqual(11, i.half_steps)

        i = Interval(IntervalQuality.DIM, 9)
        self.assertEqual('dim 10th', str(i))
        self.assertEqual(14, i.half_steps)

        i = Interval(IntervalQuality.DIM, 11)
        self.assertEqual('dim 12th', str(i))
        self.assertEqual(18, i.half_steps)

    def test_interval_aug(self):
        i = Interval(IntervalQuality.AUG, 0)
        self.assertEqual('aug unison', str(i))
        self.assertEqual(1, i.half_steps)

        i = Interval(IntervalQuality.AUG, 1)
        self.assertEqual('aug 2nd', str(i))
        self.assertEqual(3, i.half_steps)

        i = Interval(IntervalQuality.AUG, 2)
        self.assertEqual('aug 3rd', str(i))
        self.assertEqual(5, i.half_steps)

        i = Interval(IntervalQuality.AUG, 3)
        self.assertEqual('aug 4th', str(i))
        self.assertEqual(6, i.half_steps)

        i = Interval(IntervalQuality.AUG, 4)
        self.assertEqual('aug 5th', str(i))
        self.assertEqual(8, i.half_steps)

        i = Interval(IntervalQuality.AUG, 5)
        self.assertEqual('aug 6th', str(i))
        self.assertEqual(10, i.half_steps)

        i = Interval(IntervalQuality.AUG, 6)
        self.assertEqual('aug 7th', str(i))
        self.assertEqual(12, i.half_steps)

        i = Interval(IntervalQuality.AUG, 7)
        self.assertEqual('aug octave', str(i))
        self.assertEqual(13, i.half_steps)

        i = Interval(IntervalQuality.AUG, 8)
        self.assertEqual('aug 9th', str(i))
        self.assertEqual(15, i.half_steps)

        i = Interval(IntervalQuality.AUG, 10)
        self.assertEqual('aug 11th', str(i))
        self.assertEqual(18, i.half_steps)


if __name__ == '__main__':
    unittest.main()

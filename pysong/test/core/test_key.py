import unittest

from pysong.core import Key, Mode


class KeyTest(unittest.TestCase):

    def test_create_from_pc(self):
        k = Key(tonic_pitch_class=0)
        self.assertEqual(Mode.MAJOR, k.mode)
        self.assertEqual(0, k.number_of_sharps)
        self.assertEqual(0, k.tonic_pitch_class)

        k = Key(tonic_pitch_class=2)
        self.assertEqual(Mode.MAJOR, k.mode)
        self.assertEqual(2, k.number_of_sharps)
        self.assertEqual(2, k.tonic_pitch_class)

        k = Key(tonic_pitch_class=3)
        self.assertEqual(Mode.MAJOR, k.mode)
        self.assertEqual(-3, k.number_of_sharps)
        self.assertEqual(3, k.tonic_pitch_class)

        # TODO: Refactor to allow these to proceed without conversion between sharp/flat keys.
        # The following tests will need to be updated.

        # C# major converted to Db major
        k = Key(tonic_pitch_class=1)
        self.assertEqual(Mode.MAJOR, k.mode)
        self.assertEqual(-5, k.number_of_sharps)
        self.assertEqual(1, k.tonic_pitch_class)

        # Eb minor converted to D# minor
        k = Key(tonic_pitch_class=3, mode=Mode.MINOR)
        self.assertEqual(Mode.MINOR, k.mode)
        self.assertEqual(6, k.number_of_sharps)
        self.assertEqual(3, k.tonic_pitch_class)

    def test_create_from_sharps(self):
        k = Key(num_sharps=0)
        self.assertEqual(Mode.MAJOR, k.mode)
        self.assertEqual(0, k.number_of_sharps)
        self.assertEqual(0, k.tonic_pitch_class)

        k = Key(num_sharps=1)
        self.assertEqual(Mode.MAJOR, k.mode)
        self.assertEqual(1, k.number_of_sharps)
        self.assertEqual(7, k.tonic_pitch_class)

        k = Key(num_sharps=2)
        self.assertEqual(Mode.MAJOR, k.mode)
        self.assertEqual(2, k.number_of_sharps)
        self.assertEqual(2, k.tonic_pitch_class)

        k = Key(num_sharps=-3)
        self.assertEqual(Mode.MAJOR, k.mode)
        self.assertEqual(-3, k.number_of_sharps)
        self.assertEqual(3, k.tonic_pitch_class)

        # Db major
        k = Key(num_sharps=-5)
        self.assertEqual(Mode.MAJOR, k.mode)
        self.assertEqual(-5, k.number_of_sharps)
        self.assertEqual(1, k.tonic_pitch_class)

        # D# minor
        k = Key(num_sharps=6, mode=Mode.MINOR)
        self.assertEqual(Mode.MINOR, k.mode)
        self.assertEqual(6, k.number_of_sharps)
        self.assertEqual(3, k.tonic_pitch_class)


if __name__ == '__main__':
    unittest.main()

"""Custom exception type definitions."""


class ArgumentError(Exception):
    """Used to indicate incorrect use of function arguments. Simple type checking is done
    via assert statements, but when more complex logic is required we raise this exception to
    indicate bad function arguments."""


class ExtendedIntervalError(Exception):
    """Used to indicate unhandled interval qualities such as doubly augmented or diminished.
    If these come up the Interval class may need to be extended to handle; see this discussion
    https://music.stackexchange.com/questions/15430/are-doubly-augmented-and-doubly-diminished-intervals-practical
    which gives the example Gb–C# in Shostakovich (doubly-augmented 4th).
    """

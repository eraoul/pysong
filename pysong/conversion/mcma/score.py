"""Helper function to read in a score from MCMA using music21.
"""

import os
from typing import List

import music21
from pysong.conversion.music21.monophonic import make_monophonic
from pysong.core.note_sequence import NoteSequence


def get_parts_mcma(root_path: str, composer: str, collection: str, work: str) -> List[NoteSequence]:
    """Gets a list of NoteSequences from an mcma file specified by metadata (composer, collection, work)."""
    path = os.path.join(root_path, composer, collection, work + '.mxl')
    sc = music21.converter.parse(path)
    num_parts = len(sc.parts)
    mono_parts = []
    for i, part in enumerate(sc.parts):
        # measures = part.getElementsByClass('Measure')
        notes_and_chords = part.recurse().notes
        # Set the bass line to lower_note preference; higher-note preference otherwise.
        prefer_higher = i < num_parts - 1
        notes = make_monophonic(notes_and_chords, prefer_higher)
        mono_parts.append(notes)

    return mono_parts


def get_score_mcma(root_path: str, composer: str, collection: str, work: str) -> List[NoteSequence]:
    """Gets a list of NoteSequences from an mcma file specified by metadata (composer, collection, work)."""
    path = os.path.join(root_path, composer, collection, work + '.mxl')
    sc = music21.converter.parse(path)
    num_parts = len(sc.parts)
    mono_parts = []
    for i, part in enumerate(sc.parts):
        # measures = part.getElementsByClass('Measure')
        notes_and_chords = part.recurse().notes
        # Set the bass line to lower_note preference; higher-note preference otherwise.
        prefer_higher = i < num_parts - 1
        notes = make_monophonic(notes_and_chords, prefer_higher)
        mono_parts.append(notes)

    return mono_parts

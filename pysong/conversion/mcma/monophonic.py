"""Monophonic part extraction utilities"""

#
# import os
# from typing import List
#
# import music21
#
# from pysong.conversion.mcma.score import get_parts_mcma
# from pysong.conversion.music21.monophonic import make_monophonic
# from pysong.core.note_sequence import NoteSequence
#
#
# def get_monophonic_parts_mcma(root_path: str, composer: str, collection: str, work: str) -> List[NoteSequence]:
#     """Gets a list of monophonic NoteSequences from an mcma file specified by metadata (composer, collection, work).
#
#     If not monophonic, parts are converted to monophonic via heuristics.
#     """
#     parts = get_parts_mcma(root_path, composer, collection, work)
#
#     num_parts = len(parts)
#     mono_parts = []
#     for i, part in enumerate(sc.parts):
#         measures = [m for m in part.getElementsByClass('Measure')]
#         notes_and_chords = part.recurse().notes
#         # Set the bass line to lower_note preference; higher-note preference otherwise.
#         prefer_higher = True if i < num_parts - 1 else False
#         notes = make_monophonic(notes_and_chords, prefer_higher)
#         mono_parts.append(notes)
#
#     return mono_parts

"""Extract monophonic lines from music21 note/chord sequences."""

import music21
from music21.note import GeneralNote, Note as Note21
from music21.chord import Chord as Chord21

from pysong.conversion.music21.note import note21_to_note
from pysong.core.note_sequence import NoteSequence


def _note_dist(note1: Note21, note2: Note21) -> int:
    return abs(note1.pitch.midi - note2.pitch.midi)


def _max_note(note1: Note21, note2: Note21) -> Note21:
    if note1.pitch.midi > note2.pitch.midi:
        return note1
    return note2


def _min_note(note1: Note21, note2: Note21) -> Note21:
    if note1.pitch.midi < note2.pitch.midi:
        return note1
    return note2


def _pick_note(note_or_chord: GeneralNote, prev: Note21, bias_higher_notes: bool) -> Note21:
    # If we have a note, just use it.
    if note_or_chord.isNote:
        assert isinstance(note_or_chord, Note21)
        return note_or_chord

    # If we have a chord, make a decision.
    assert isinstance(note_or_chord, Chord21)
    chord = note_or_chord

    if not prev:
        # No previous note; use top or bottom note as desired.
        if bias_higher_notes:
            return chord[0]
        return chord[-1]

    # We have a previous note, so find the closest items in chord to it.
    chord.sortAscending(inPlace=True)

    argmin = chord[0]
    min_dist = _note_dist(chord[0], prev)
    for note in chord.notes[1:]:
        dist = _note_dist(note, prev)
        if dist < min_dist:
            min_dist = dist
            argmin = note
        if dist == min_dist:
            if bias_higher_notes:
                argmin = _max_note(argmin, note)
            else:
                argmin = _min_note(argmin, note)
    return argmin


def make_monophonic(notes_and_chords: music21.stream.Stream, bias_higher_notes=True) -> NoteSequence:
    """Given a stream returned by music21 Stream.notes, which may include Note or Chord objects,
    convert to notes only. Follow the melody left to right and pick closest note to previous, or highest
    if bias_higher_note==True (picks lowest note, if False).

    Returns a new pysong NoteSequence stream containing the notes.
    TODO: return a sequence of measures instead.
    """
    prev_note = None

    notes = NoteSequence()
    for nc in notes_and_chords:
        note21 = _pick_note(nc, prev_note, bias_higher_notes)
        note = note21_to_note(note21)
        notes.append(note)
        prev_note = note21
    return notes

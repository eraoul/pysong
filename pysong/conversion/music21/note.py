"""Convert between music21 and pysong Note classes."""

from music21.note import Note as Note21  # GeneralNote

from pysong.conversion.music21.duration import to_duration
from pysong.core.note import Note
from pysong.core.note import Accidental


def to_accidental(note21: Note21) -> Accidental:
    """Return the Accidental corresponding to the accidental of the given music21 Note."""
    if not note21.pitch.accidental:
        return Accidental.Natural
    a = note21.pitch.accidental.name
    if a == 'flat':
        return Accidental.Flat
    if a == 'sharp':
        return Accidental.Sharp
    if a == 'natural':
        return Accidental.Natural
    if a == 'double-sharp':
        return Accidental.DoubleSharp
    if a == 'double-flat':
        return Accidental.DoubleFlat

    raise Exception(f'unknown accidental {a}')


def note21_to_note(note: Note21) -> Note:
    """Construct a new pysong Note from the given music21 note."""
    name = note.pitch.name[0]
    acc = to_accidental(note)
    if note.tie:
        tie = note.tie.type == 'start' or note.tie.type == 'continue'
    else:
        tie = False
    return Note(name, acc, note.octave, to_duration(note), tie_to_next=tie)

"""Convert from a music21 duration to a pysong duration."""

from fractions import Fraction

from music21.note import Note as Note21

from pysong.core.duration import Duration


def to_duration(note21: Note21) -> Duration:
    """Given a music21 note, return is duration as a pysong Duration."""
    q = note21.duration.quarterLength
    f = Fraction(q).limit_denominator(128)
    return Duration(f.numerator, f.denominator * 4)

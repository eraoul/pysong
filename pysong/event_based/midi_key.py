"""Represents a Key for MIDI files."""

from pysong.exceptions import ArgumentError
from pysong.core.key import Mode

# Lookup tables mapping from pitch class (0-11) to string name of key.
_PC_TO_NAME_MAJOR = ('C', 'Db', 'D', 'Eb', 'E', 'F', 'Gb', 'G', 'Ab', 'A', 'Bb', 'B')
_PC_TO_NAME_MINOR = ('C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'Bb', 'B')

# Lookup tables mapping from # sharps to pitch class and vice versa.
_SHARPS_TO_TONIC_MAJOR = {i: (7 * i) % 12 for i in range(-6, 7)}
_SHARPS_TO_TONIC_MINOR = {i: (7 * i - 3) % 12 for i in range(-6, 7)}
_TONIC_TO_SHARPS_MAJOR = {_SHARPS_TO_TONIC_MAJOR[i]: i for i in range(-6, 7)}
_TONIC_TO_SHARPS_MINOR = {_SHARPS_TO_TONIC_MINOR[i]: i for i in range(-6, 7)}


class MidiKey:
    """Represents a musical key signature."""

    def __init__(self, tonic_pitch_class=None, num_sharps=None, mode=Mode.MAJOR):
        """Must specify either tonic_pitch_class or num_sharps.
        tonic_pitch_class: 0-11. 0=C, 1=C#, etc.
        num_sharps: positive number for # sharps. Negative number represents # of flats.
        mode: Mode enum. MAJOR or MINOR.

         # TODO: using a pitch class for tonic doesn't work for nonstandard notation including Bach's WTC C# Major
         # pieces (such as BWV 848). For these we need to specify the tonic note and accidental directly.
        """
        if ((tonic_pitch_class is None and num_sharps is None)
                or (tonic_pitch_class is not None and num_sharps is not None)):
            raise ArgumentError('Specify exactly one of {tonic_pitch_class, num_sharps}')

        assert isinstance(mode, Mode)
        self.mode = mode

        if tonic_pitch_class is not None:
            assert 0 <= tonic_pitch_class < 12
            self.tonic_pitch_class = tonic_pitch_class
        else:
            assert isinstance(num_sharps, int)
            assert -7 < num_sharps < 7
            if mode == Mode.MAJOR:
                self.tonic_pitch_class = _SHARPS_TO_TONIC_MAJOR[num_sharps]
            else:
                self.tonic_pitch_class = _SHARPS_TO_TONIC_MINOR[num_sharps]

    def __repr__(self):
        if self.mode == Mode.MAJOR:
            name_map = _PC_TO_NAME_MAJOR
        else:
            name_map = _PC_TO_NAME_MINOR
        return '%s %s' % (name_map[self.tonic_pitch_class], self.mode.name)

    def __eq__(self, key2):
        return isinstance(key2, MidiKey) and self.tonic_pitch_class == key2.tonic_pitch_class and self.mode == key2.mode

    def number_of_sharps(self):
        """Returns the number of sharps in the key signature, or negative numbers for the number
        of flats, as in MIDI key signatures."""
        if self.mode == Mode.MAJOR:
            return _TONIC_TO_SHARPS_MAJOR[self.tonic_pitch_class]
        return _TONIC_TO_SHARPS_MINOR[self.tonic_pitch_class]

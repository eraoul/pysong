from enum import Enum

from pysong.event_based.midi_measure import MidiMeasure


class TrackType(Enum):
    """Enum to represent simplified pop-style type of a track (Melody/Harmony/etc.)"""
    UNKNOWN = 0
    MELODY = 1
    HARMONY = 2
    BASS = 3
    DRUMS = 4
    FX = 5


class Track:
    """Stores a single track (instrument) in a Song. Consists of multiple measures."""

    def __init__(self, name, program, channel, track_type, parent_song):
        self.name = name
        self.program = program  # MIDI program number
        self.channel = channel  # MIDI channel

        # Melody, harmony, bass, or drums.
        assert isinstance(track_type, TrackType)
        self.track_type = track_type

        assert isinstance(parent_song, Core.song.Song)
        self.parent_song = parent_song
        self.measures = []

    def __iter__(self):
        return iter(self.measures)

    def __eq__(self, t2):
        if (not (isinstance(t2, Track) and self.name == t2.name and self.program == t2.program
                 and self.channel == t2.channel and self.track_type == t2.track_type
                 and len(self.measures) == len(t2.measures))):
            return False
        for i, measure in enumerate(self.measures):
            if not measure == t2.measures[i]:
                return False
        return True

    def __repr__(self):
        return '%s (program:%d, channel=%d, type=%s)' % (self.name, self.program, self.channel, self.track_type.name)

    def new_measure(self, time_signature=None):
        """Adds a new measure at the end of the track and returns the measure."""
        next_tick = 0
        if self.measures:
            prev_measure = self.measures[-1]
            next_tick = prev_measure.start_tick + prev_measure.get_duration_ticks()
        measure = MidiMeasure(next_tick, self, time_signature)
        self.measures.append(measure)
        return measure

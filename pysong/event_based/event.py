"""Musical timslice-based events."""

from pysong.event_based.midi_note import MidiNote


class Event:
    """Stores a single timeslice Event in a single track. This consists of all notes sounding at a particular
    moment in time, and a duration. This comprises in general multiple MIDI events."""

    def __init__(self, duration=0):
        self.notes = []
        self.duration = int(duration)  # in ticks (as defined in the ancestor Song object)

    def __iter__(self):
        return iter(self.notes)

    def __eq__(self, e2):
        if not (isinstance(e2, Event) and self.duration == e2.duration and len(self.notes) == len(e2.notes)):
            return False

        # Don't require notes to be in same order. Treat as a set.
        for note in self.notes:
            for note2 in e2.notes:
                if note == note2:
                    break
            else:
                return False
        return True

    def __repr__(self):
        s = 'Event duration: %d [' % self.duration
        for i, note in enumerate(self.notes):
            s += str(note)
            if i < len(self.notes) - 1:
                s += ', '
        s += ']'
        return s

    def append_note(self, note):
        """Append a single note to the event."""
        self.notes.append(note)

    def new_note(self, midi_pitch, velocity=-1, tie_from_previous=False):
        """Make a new note and append to the event."""
        note = MidiNote(midi_pitch, velocity, tie_from_previous)
        self.notes.append(note)
        return note

"""Core classes used for an event-based and MIDI-based representation of music.
"""

from .event import Event
from .midi_key import MidiKey
from .midi_measure import MidiMeasure
from .midi_note import MidiNote
from .track import Track, TrackType

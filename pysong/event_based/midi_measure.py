"""Represents a measure in MIDI."""

from copy import deepcopy

from pysong.core.time_signature import TimeSignature
from pysong.event_based.event import Event
from pysong.event_based.track import Track


class MidiMeasure:
    """Stores a single measure of a single track. Consists of multiple Events."""

    def __init__(self, start_tick, parent_track, time_signature=None):
        assert isinstance(parent_track, Track)
        self.events = []
        self.start_tick = start_tick
        self.parent_track = parent_track
        if time_signature:
            self.time_signature = time_signature
        else:
            self.time_signature = parent_track.parent_song.time_signature
        assert isinstance(self.time_signature, TimeSignature)

    def __iter__(self):
        return iter(self.events)

    def __eq__(self, m2):
        if not isinstance(m2, MidiMeasure):
            return False

        # Ignore any final rests in a measure in comparison.
        events1 = deepcopy(self.events)
        events2 = deepcopy(m2.events)
        len_events1 = len(events1)
        for i in range(len(events1) - 1, -1, -1):
            if not events1[i].notes:
                len_events1 = i
            else:
                # As soon as we see notes, break.
                break

        len_events2 = len(events2)
        for i in range(len(events2) - 1, -1, -1):
            if not events2[i].notes:
                len_events2 = i
            else:
                # As soon as we see notes, break.
                break

        if len_events1 != len_events2:
            return False

        for i in range(0, len_events1):
            if not self.events[i] == m2.events[i]:
                return False
        return True

    def __repr__(self):
        s = 'Measure:'
        for event in self:
            s += '\n\t%s' % str(event)
        return s

    def new_event(self, duration):
        """Create a new Event, add to the measure, and return."""
        event = Event(duration)
        self.events.append(event)
        return event

    def get_duration_ticks(self):
        """Returns the length of the measure in ticks, based on the time signature."""
        beats = self.time_signature.numerator * 4.0 / self.time_signature.denominator
        return int(beats * self.parent_track.parent_song.ticks_per_beat)

"""Represents a note in MIDI."""

from functools import total_ordering

FLAT = '\u266D'
NATURAL = '\u266E'
SHARP = '\u266F'


def unicode_note_name(note: str):
    """Return a unicode-formatted version of the note name."""
    return note.replace('b', FLAT).replace('#', SHARP)


@total_ordering
class MidiNote:
    """Stores a single Note. This is either a note on event or a continuation of a previously
    sounding note. Note off events are handled implicitly by the presence of a later event where
    the note is not sounding."""

    def __init__(self, midi_pitch, velocity=-1, tie_from_previous=False):
        self.midi_pitch = midi_pitch
        self.velocity = velocity  # set to -1 if tie_from_previous==True
        self.tie_from_previous = tie_from_previous

    def __eq__(self, n2):
        return (isinstance(n2, MidiNote) and self.midi_pitch == n2.midi_pitch and self.velocity == n2.velocity
                and self.tie_from_previous == n2.tie_from_previous)

    def __lt__(self, n2):
        assert isinstance(n2, MidiNote)
        return self.midi_pitch < n2.midi_pitch

    def __repr__(self):
        tie_string = ''
        if self.tie_from_previous:
            tie_string = 'TIE'
        return '%d[v=%d %s]' % (self.midi_pitch, self.velocity, tie_string)

# from core.note import *
# from core.measure import *
# from core.measure_sequence import *
# from core.key import *
# from core.duration import *

from music21.meter import TimeSignature as Music21TimeSignature
from pysong import Accidental, Chord, Quarter, Half, Eighth, Key, Measure, MeasureSequence, Mode, Note, Rest, _STRING_TO_ACCIDENTAL

from copy import copy
import random

from typing import List


def start_chord_and_key():
    # return ['C', 'E', 'G'], Key(num_sharps=0)
    return ['E', 'G', 'B'], Key(num_sharps=1, mode=Mode.MINOR)


def midi_to_note(midi, duration, octave=4, key=None):
    note_name = Note.midi_to_note_name(midi, key=key)

    # handle accidentals
    if len(note_name) > 1:
        accidental_str = note_name[1:]
        note_name = note_name[0]
        accidental = _STRING_TO_ACCIDENTAL[accidental_str]
    else:
        accidental = Accidental.Natural

    return Note(note_name=note_name, octave=octave, accidental=accidental, duration=duration)


def note_name_to_midi(note_name: str) -> int:
    return Note(note_name).midi_pitch


def intervals_too_small(midis: List[int], min_interval: int) -> bool:
    sorted_pcs = sorted([m % 12 for m in midis])
    for i in range(1, len(sorted_pcs)):
        if abs(sorted_pcs[i] - sorted_pcs[i - 1]) < min_interval:
            return True
    if abs(sorted_pcs[-1] - sorted_pcs[0]) < min_interval:
        return True
    return False


MAX_ATTEMPTS = 10  # avoid infinite loop due to edge cases when changing min_interval.


def new_chord(prev_midis: List[int], key: Key, min_interval: int = 3) -> List[int]:
    """Give a chord in midi note numbers, make a random modification
    based on transition probabilities etc.  Return the new midi.

    So far we are ignoring octaves and looking at pcs instead.
    """
    # pick one midi and modify if randomly up or down by 1 or 2.
    # return the modified list of midis with jsut that one element
    # modified.
    found = False
    prev_pcs = [m % 12 for m in prev_midis]
    prev_pc = -1

    attempts = 0
    while not found:
        # pick a random midi but make sure it's not the same as the previous
        idx = random.randint(0, len(prev_pcs) - 1)
        pc = prev_pcs[idx]
        if pc == prev_pc:
            continue

        # midi = prev_midis[idx]
        modification = random.randint(-1, 2)
        # Make sure we return -2, -1, 1, or 2
        if modification < 0:
            modification -= 1
        # new_midi = (midi + modification)
        new_pc = (pc + modification) % 12

        found = True
        if new_pc in prev_pcs:
            found = False
            continue

        new_pcs = copy(prev_pcs)
        new_pcs[idx] = new_pc

        if intervals_too_small(new_pcs, min_interval) and attempts < MAX_ATTEMPTS:
            found = False
            continue

        prev_pc = pc
        prev_pcs = sorted(new_pcs)

    return new_pcs


CHORD_ONLY = False


def main():
    print('Generating chords...')

    start_chord, k = start_chord_and_key()
    print(start_chord, k)
    midis = [note_name_to_midi(n) for n in start_chord]

    ms = MeasureSequence()
    m = Measure()

    for measure_num in range(1, 17):
        # Treat the midis as pcs, and rotate so root is on bottom.
        pcs = sorted(m % 12 for m in midis)
        # find root: this is the upper pc of the largest interval, including
        # the interval between last and first notes.
        pcs.append(pcs[0] + 12)
        # compute intervals.
        intervals = [abs(pcs[i] - pcs[i - 1]) for i in range(1, len(pcs))]
        # find the index of the  largest interval.
        max_idx = (intervals.index(max(intervals)) + 1) % len(pcs)
        # rorate the list so that max_idx is at the start of the list.
        # print(pcs, intervals, pcs)
        pcs = pcs[max_idx:-1] + pcs[:max_idx]
        # print(pcs)
        # now compute an octave for each pc. If a pc is below the previous one,
        # add an octave. Start on octave 4.

        octaves = [4]
        for i in range(1, len(pcs)):
            if pcs[i] < pcs[i - 1]:
                octaves.append(octaves[-1] + 1)
            else:
                octaves.append(octaves[-1])
        k = Key(pcs[0])
        notes = [midi_to_note(pc, duration=Half, octave=octave, key=k) for pc, octave in zip(pcs, octaves)]

        min_interval = 3
        if measure_num % 4 == 0:
            min_interval = 3
        if measure_num % 4 == 1:
            min_interval = 3
        if measure_num % 4 == 2:
            min_interval = 1
        if measure_num % 4 == 3:
            min_interval = 2

        if CHORD_ONLY:
            c = Chord(notes, duration=Half)
            m.append_note(c)
            # if measure_num % 2 == 0:
        else:
            # Arpeggiate the notes.
            n = copy(notes[0])
            n.octave = 2
            n.duration = Quarter

            if measure_num % 4 == 1 or measure_num % 4 == 3:
                pattern = 1
            elif measure_num % 4 == 2:
                pattern = 2
            else:
                pattern = 3

            if pattern == 1:
                m.append_note(n)

                for i in range(1, 8):
                    n = copy(notes[i % len(notes)])
                    n.octave = 3 + i//len(notes)
                    if 0 < i < 3:
                        n.duration = Eighth
                    elif i == 7:
                        n.duration = Half
                    else:
                        n.duration = Quarter
                    m.append_note(n)

            elif pattern == 2:
                n.duration = Half
                m.append_note(n)
                for i in range(1, 5):
                    n = copy(notes[i % len(notes)])
                    n.octave = 3 + i//len(notes)
                    if 0 < i < 3:
                        n.duration = Quarter
                    else:
                        n.duration = Half
                    m.append_note(n)
            elif pattern == 3:
                m.append_note(n)
                for i in range(1, 9):
                    n = copy(notes[i % len(notes)])
                    n.octave = int(5.3 - 0.1 * (i - 5)**2)
                    if i > 6:
                        n.duration = Eighth
                    else:
                        n.duration = Quarter
                    m.append_note(n)
        ms.append(m)
        m = Measure()

        # update the midis.
        # print(f'{midis=}')
        midis = new_chord(prev_midis=midis, key=k, min_interval=min_interval)

    ms.show(time_signature=Music21TimeSignature('2/4'))


if __name__ == '__main__':
    import music21

    us = music21.environment.UserSettings()
    us['musicxmlPath'] = '/Applications/MuseScore 4.app/Contents/MacOS/mscore'
    # us['musescoreDirectPNGPath'] = 'C:\\Program Files\\MuseScore 3\\bin\\MuseScore3.exe'
    # us['musicxmlPath']

    main()
